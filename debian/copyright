Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: sysrepo
Source: https://github.com/sysrepo/sysrepo

Files: *
Copyright: 2015-2020 CESNET, z.s.p.o.
	   2016-2020 Deutsche Telekom AG.
License: Apache-2.0

Files: modules/ietf-datastores*
Copyright: 2018 IETF Trust
		Martin Bjorklund
		Juergen Schoenwaelder
		Phil Shafer
		Kent Watsen
		Rob Wilton
License: BSD-3-clause

Files: modules/ietf-netconf-notification.yang modules/ietf_datastores_yang.h
Copyright: 2012 IETF Trust
		Andy Bierman
License: BSD-3-clause

Files: modules/ietf-netconf-with-defaults.yang modules/ietf_netconf_with_defaults_yang.h
Copyright: 2011 IETF Trust
		Andy Bierman
		Balazs Lengyel
License: BSD-3-clause

Files: modules/ietf-netconf.yang modules/ietf_netconf_yang.h
Copyright: 2011 IETF Trust
		Martin Bjorklund
		Juergen Schoenwaelder
		Andy Bierman
License: BSD-3-clause

Files: modules/ietf-origin.yang modules/ietf_origin_yang.h
Copyright: 2018 IETF Trust
		Martin Bjorklund
		Juergen Schoenwaelder
		Phil Shafer
		Kent Watsen
		Rob Wilton
License: BSD-3-clause

Files: modules/ietf-yang-library@2016-06-21.yang modules/ietf_yang_library@2016_06_21_yang.h
Copyright: 2016 IETF Trust
		Andy Bierman
		Martin Bjorklund
		Kent Watsen
License: BSD-3-clause

Files: modules/ietf-yang-library@2019-01-04.yang modules/ietf_yang_library@2019_01_04_yang.h
Copyright: 2011 IETF Trust
		Andy Bierman
		Martin Bjorklund
		Juergen Schoenwaelder
		Kent Watsen
		Robert Wilton
License: BSD-3-clause

Files: bindings/python tests/measure_performance.c
Copyright: 2016 Cisco Systems, Inc.
	   2018 Deutsche Telekom AG
	   2019 CESNET, z.s.p.o.
License: Apache-2.0

Files: CMakeModules/FindCMocka.cmake
Copyright: 2011-2012 Andreas Schneider
License: BSD-3-clause

License: Apache-2.0
 Licensed to the Apache Software Foundation (ASF) under one or more
 contributor license agreements.  See the NOTICE file distributed with
 this work for additional information regarding copyright ownership.
 The ASF licenses this file to You under the Apache License, Version 2.0
 (the "License"); you may not use this file except in compliance with
 the License.  You may obtain a copy of the License at
 .
      https://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the full text of the Apache Software License version 2 can
 be found in the file `/usr/share/common-licenses/Apache-2.0'.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
